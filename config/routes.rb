Rails.application.routes.draw do
  get 'about' => 'static_pages#about'
  get 'opensource' => 'static_pages#opensource'

  root 'static_pages#home'
end
